﻿﻿using UnityEngine;
using System.Collections;
using System.Text;
#if UNITY_EDITOR
using UnityEditor;
#endif
#if !UNITY_5
using UnityEngine.Profiling;
#endif

/// <summary>
/// A Unity profiling tool used to monitoring main stats on different devices without 
/// the need to always connect to profile: Current FPS and frame time, Average FPS, GPU
/// Memory Usage, Total Allocated Memory, Total Reserved Memory, Total UnusedReserved 
/// Memory For editor mode only It will show DrawCalls, used texture memory, renderedTextureCount.
/// 
/// Tested on Android and IOS, intended to work with Unity 5.x, attach current script to camera for usage.  
/// 
/// Ref: https://github.com/ahmedmohi/iProfiler
/// </summary>
public class Stat : MonoBehaviour
{
	#region Public Config Variable
	[Tooltip("The style used for GUI text display.")]
	public GUIStyle style = new GUIStyle();
	[Tooltip("Frame rate counter over duration(s), update per second.")]
	public int[] intervals = new int[] { 5, 15, 60 };
	[Tooltip("Small number format for displaying frame rate.")]
	public string numberFormat = "0.000";
	#endregion

	#region Private Data Holder Variable
	// GUI display variables
	private StringBuilder tx = new StringBuilder();
	private string stat = null;

	// Frame rate variables
	private int frameCount = 0;
	private float lastTimeStamp = 0.0f;
	private const float interval = 1.0f;
	private float framePerSecond = 0.0f;

	private int frameRateCounter = 0;
	private float[] frameTotals = { 0.0f };
	private int framesCachePointer = 0;
	private float[] frameCaches = { 0.0f };
	private string[] frameRates = { };

	public long textureMemory = 0;
	public long audioMemory = 0;
	public long meshMemory = 0;
	public long materialMemory = 0;
	public long gameObjectMemory = 0;
	public long componentMemory = 0;
	#endregion

	#region Constructor
	/// <summary>
	/// Use this for initialization
	/// </summary>
	void Start()
	{
		frameCount = frameRateCounter = 0;
		lastTimeStamp = Time.realtimeSinceStartup;
		framePerSecond = 0;
		frameTotals = new float[intervals.Length];
		frameRates = new string[intervals.Length];
		framesCachePointer = 0;
		frameCaches = new float[Mathf.Max(intervals)];
		textureMemory = audioMemory = meshMemory = materialMemory = gameObjectMemory = componentMemory = 0;
	}
	#endregion

	#region Frame Update
	/// <summary>
	/// On GUI update, draw debug information.  Feature to be removed
	/// by future version of Unity
	/// </summary>
	void OnGUI()
	{
		// Display stat
		GUI.Label(new Rect(10, 10, 600, 400), stat, style);
	}

	/// <summary>
	/// Update is called once per frame
	/// </summary>
	void Update()
	{
		// Update frame counter
		frameCount++;
		float timeStamp = Time.realtimeSinceStartup;
		if (timeStamp > lastTimeStamp + interval)
		{
			framePerSecond = (float)frameCount / (timeStamp - lastTimeStamp);
			frameCount = 0;
			lastTimeStamp = Time.realtimeSinceStartup;

			// Update additional timers
			int tempPtr = -1;
			frameRateCounter++;
			for (int i = 0; i < intervals.Length; ++i)
			{
				frameTotals[i] += framePerSecond;
				if (frameRateCounter > intervals[i])
				{
					tempPtr = framesCachePointer - intervals[i];
					frameTotals[i] -= frameCaches[(tempPtr < 0 ? tempPtr + frameCaches.Length : tempPtr)];
					frameRates[i] = (frameTotals[i] / intervals[i]).ToString(numberFormat);
				}
				else
					frameRates[i] = (frameTotals[i] / frameRateCounter).ToString(numberFormat);
			}
			// Cache frame data
			frameCaches[framesCachePointer] = framePerSecond;
			if (++framesCachePointer >= frameCaches.Length) framesCachePointer -= frameCaches.Length;

			// Calculate texture memory
			StartCoroutine(CalculateMemoryUsage(frameRateCounter));

			// Dump to display
			tx.Length = 0;
			tx.AppendFormat("Frame Time: {0}ms | Frame Rate: {1}/sec | Timestamp: {2}sec \n" +
				"Interval Rates: ({3})/sec\n" +
				"GPU Memory: {4}MB | Sys Memory: {5}MB\n" +
				"Allocated Memory: {6}MB | Reserved Memory: {7}MB | Unused Memory: {8}MB\n" +
				"Runtime Memory: {{Texture: {9}, Audio: {10}, Mesh: {11}, Material: {12}, GameObject: {13}, Component: {14} }}MB\n"
				,
				(1000.0f / Mathf.Max(framePerSecond, 0.00001f)).ToString(numberFormat),
				framePerSecond.ToString(numberFormat), timeStamp.ToString(numberFormat),
				string.Join(", ", frameRates),
				SystemInfo.graphicsMemorySize, SystemInfo.systemMemorySize,
				ByteToMegabyte(Profiler.GetTotalAllocatedMemory(), numberFormat),
				ByteToMegabyte(Profiler.GetTotalReservedMemory(), numberFormat),
				ByteToMegabyte(Profiler.GetTotalUnusedReservedMemory(), numberFormat),
//#if !UNITY_5
//				ByteToMegabyte(Profiler.GetTotalAllocatedMemoryLong(), numberFormat),
//				ByteToMegabyte(Profiler.GetTotalReservedMemoryLong(), numberFormat),
//				ByteToMegabyte(Profiler.GetTotalUnusedReservedMemoryLong(), numberFormat),
//#endif
				ByteToMegabyte(textureMemory, numberFormat),
				ByteToMegabyte(audioMemory, numberFormat),
				ByteToMegabyte(meshMemory, numberFormat),
				ByteToMegabyte(materialMemory, numberFormat),
				ByteToMegabyte(gameObjectMemory, numberFormat),
				ByteToMegabyte(componentMemory, numberFormat)
			);
#if UNITY_EDITOR
			tx.AppendFormat("Draw Calls : {0} | Used Texture Memory : {1} | RenderedTextureCount : {2}",
				UnityStats.drawCalls, UnityStats.usedTextureMemorySize / 1048576, UnityStats.usedTextureCount);
#endif
			stat = tx.ToString();
		}
		return;
	}
	#endregion

	#region Helper Method
	/// <summary>
	/// Convert byte into megabyte string
	/// </summary>
	/// <param name="b">Number value in bytes</param>
	/// <param name="format">String format</param>
	/// <returns>String formate in megabyte</returns>
	private string ByteToMegabyte (long b, string format = "")
	{
		return (b / 1048576.0f).ToString(format);
	}

	/// <summary>
	/// Calculate total memory usage by texture, audio, mesh, material, game object, and component
	/// </summary>
	/// <param name="index">A task spliter, 0 ~ 5, to reduce the straint on resources</param>
	/// <remarks>Ref: https://docs.unity3d.com/ScriptReference/Resources.FindObjectsOfTypeAll.html </remarks>
	/// <returns>Thread flag</returns>
	private IEnumerator CalculateMemoryUsage(int index)
	{
		yield return -1;

		// Task splitter
		switch (index % 6)
		{
			case 0:
				textureMemory = 0;
				foreach (Texture t in Resources.FindObjectsOfTypeAll(typeof(Texture)))
				{
					textureMemory += Profiler.GetRuntimeMemorySizeLong((Texture)t);
				}
				break;
			case 1:
				audioMemory = 0;
				foreach (AudioClip t in Resources.FindObjectsOfTypeAll(typeof(AudioClip)))
				{
					audioMemory += Profiler.GetRuntimeMemorySizeLong((AudioClip)t);
				}
				break;
			case 2:
				meshMemory = 0;
				foreach (Mesh t in Resources.FindObjectsOfTypeAll(typeof(Mesh)))
				{
					meshMemory += Profiler.GetRuntimeMemorySizeLong((Mesh)t);
				}
				break;
			case 3:
				materialMemory = 0;
				foreach (Material t in Resources.FindObjectsOfTypeAll(typeof(Material)))
				{
					materialMemory += Profiler.GetRuntimeMemorySizeLong((Material)t);
				}
				break;
			case 4:
				gameObjectMemory = 0;
				foreach (GameObject t in Resources.FindObjectsOfTypeAll(typeof(GameObject)))
				{
					gameObjectMemory += Profiler.GetRuntimeMemorySizeLong((GameObject)t);
				}
				break;
			case 5:
				componentMemory = 0;
				foreach (Component t in Resources.FindObjectsOfTypeAll(typeof(Component)))
				{
					componentMemory += Profiler.GetRuntimeMemorySizeLong((Component)t);
				}
				break;
		}
	}
	#endregion
}